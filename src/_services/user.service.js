import axios from "axios";
import { endpoints } from "./endpoints";
import { authHeader, getError } from "../_helpers";
import { method, result } from "lodash";

/**
 * Functions used to issue AJAX requests and manage responses.
 * All of the included methods use the Axios library for Promise-based requests.
 */
export const userService = {
  login,
  logout,
  operator
};

function login(username, password) {
  const encodedUser = btoa(username + ":" + password);

 getToken();
 var token = JSON.parse(localStorage.getItem("resp")).access_token;
  
 return axios
    .get(endpoints.BASEURL + endpoints.AUTH, {
      headers: { Authorization: "Bearer " + token 
    }
    })
    .then(function(response) {
      localStorage.setItem("user", encodedUser);
      return encodedUser;
    })
    .catch(function(error) {
      return Promise.reject(getError(error));
    });


}

export function getToken()
{
  var request = require('request');
  var resp;
  var options = {
    'method': 'POST',
    'url': 'https://ingbanknv01.pegalabs.io/prweb/PRRestService/oauth2/v1/token',
    'headers': {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'JSESSIONID=3FCAF9828AB604DB49B1E44EA6A3F999'
    },
    form: {
      'client_id': '17012771392032743996',
      'client_secret': '0314FADD4CA9DA859F79B1879F909377',
      'grant_type': 'client_credentials'
    }
  };
  request(options, function (error, response) {
    if (error) throw new Error(error);
    var responseObject = JSON.parse(response.body);
    localStorage.setItem("resp", response.body);
    return resp;
  });

}

function logout() {
  localStorage.removeItem("user");
}

function operator() {
  return axios
    .get(endpoints.BASEURL + endpoints.DATA + "/D_OperatorID", {
      headers: authHeader()
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return Promise.reject(getError(error));
    });
}
